﻿const uri = 'api/comments';
let comments = [];
let rawData = undefined;

function getItems() {
    fetch(uri)
        .then(response => response.json())
        .then(data => { rawData = data; sortComments()})
        .catch(error => console.error('Unable to get items.', error));
}

function sortComments() {
    let value = document.getElementById('sorting').value;

    switch (value) {
        case '1':
            rawData = rawData.sort((a, b) => {
                if (a.dateTime > b.dateTime) {
                    return -1;
                } else {
                    return 1;
                }
            });
            break;
        case '2':
            rawData = rawData.sort((a, b) => {
                if (a.dateTime > b.dateTime) {
                    return 1;
                } else {
                    return -1;
                }
            });
            break;
        case '3':
            rawData = rawData.sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                } else {
                    return 1;
                }
            });
            break;
        case '4':
            rawData = rawData.sort((a, b) => {
                if (a.name < b.name) {
                    return 1;
                } else {
                    return -1;
                }
            });
            break;
        default:
            console.log(`Error`);
    }

    _displayItems(rawData);
}

function _displayItems() {
    const section = document.getElementById('agencies');
    section.innerHTML = '';

    //_displayCount(data.length);

    //const button = document.createElement('button');

    rawData.forEach(item => {
        item.dateTime = new Date(item.dateTime);
        let cardDiv = document.createElement('div');
        cardDiv.classList.add('card', 'ts-item', 'ts-item__list', 'ts-item__company', 'ts-card');

        let cardBodyDiv = document.createElement('div');
        cardBodyDiv.classList.add('card-body', 'comments');

        let cardFigure = document.createElement('figure');
        cardFigure.classList.add('ts-item__info');

        let figureHeading = document.createElement('h4');
        figureHeading.innerText = `${item.name} - ${item.email}`;

        let figureSubHeading = document.createElement('aside');
        let figureSubheadingIcon = document.createElement('i');
        figureSubheadingIcon.classList.add('fa', 'fa-calendar', 'mr-2');

        figureSubHeading.append(figureSubheadingIcon, new Text(item.dateTime.toDateString()));
        cardFigure.append(figureHeading, figureSubHeading);

        let commentTextWrapper = document.createElement('div');
        commentTextWrapper.classList.add('ts-company-info');

        let commentDiv = document.createElement('div');
        commentDiv.classList.add('ts-company-contact', 'mb-2', 'mb-sm-0');
        commentDiv.innerText = item.text;

        commentTextWrapper.appendChild(commentDiv);

        cardBodyDiv.append(cardFigure, commentTextWrapper);

        let cardFooter = document.createElement('div');
        cardFooter.classList.add('card-footer', 'comments');

        let cardFooterEdit = document.createElement('a');
        cardFooterEdit.classList.add('ts-btn-arrow');
        cardFooterEdit.innerText = 'Edit';
        cardFooterEdit.href = `editComment.html?id=${item.id}`;

        cardFooter.appendChild(cardFooterEdit);

        cardDiv.append(cardBodyDiv, cardFooter);
        section.appendChild(cardDiv);
    });

    comments = rawData;
}