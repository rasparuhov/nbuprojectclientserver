﻿const uri = 'api/comments';
let comment = {};
let id = undefined;

function getItem() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    // Get the value of "some_key" in eg "https://example.com/?some_key=some_value"

    id = params.id;

    if (id) {
        _initialiseForEdit(params.id);
    } else {
        let deleteButton = document.getElementById('form-contact-delete');
        deleteButton.classList.add('display-none');
    }

}

function sendForm() {
    if (id) {
        _updateComment();
    } else {
        _createComment();
    }
}

function _initialiseForEdit(id) {
    let heading = document.getElementById('comment-heading');
    heading.innerText = 'Edit Comment';

    let saveButton = document.getElementById('form-contact-submit');
    saveButton.innerText = 'Save';

    fetch(`${uri}/${id}`)
        .then(response => response.json())
        .then(data => _displayItem(data))
        .catch(error => console.error('Unable to get items.', error));
}

function _displayItem(data) {
    let nameField = document.getElementById('form-contact-name');
    nameField.value = data.name;

    let emailField = document.getElementById('form-contact-email');
    emailField.value = data.email;

    let messageField = document.getElementById('form-contact-message');
    messageField.value = data.text;

    comment = data;
}

function _createComment() {
    comment = {};
    let nameField = document.getElementById('form-contact-name');
    comment.name = nameField.value;

    let emailField = document.getElementById('form-contact-email');
    comment.email = emailField.value;

    let messageField = document.getElementById('form-contact-message');
    comment.text = messageField.value;

    let secretField = document.getElementById('form-contact-secret');
    comment.secret = secretField.value;

    fetch(`${uri}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(comment)
    })
        .then(() => location.assign('/comments.html'))
        .catch(error => console.error('Unable to update item.', error));
}

function _updateComment() {
    let nameField = document.getElementById('form-contact-name');
    comment.name = nameField.value;

    let emailField = document.getElementById('form-contact-email');
    comment.email = emailField.value;

    let messageField = document.getElementById('form-contact-message');
    comment.text = messageField.value;

    let secretField = document.getElementById('form-contact-secret');
    comment.secret = secretField.value;

    fetch(`${uri}/${id}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(comment)
    })
        .then(() => location.assign('/comments.html'))
        .catch(error => console.error('Unable to update item.', error));
}

function deleteComment() {
    let secretField = document.getElementById('form-contact-secret');
    fetch(`${uri}/${id}?secret=${secretField.value}`, {
        method: 'DELETE'
    })
        .then(() => location.assign('/comments.html'))
        .catch(error => console.error('Unable to delete item.', error));
}