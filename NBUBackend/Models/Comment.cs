﻿namespace NBUBackend.Models
{
    public class Comment
    {
        public long Id { get; set; }
        public string Text { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string Secret { get; set; } = null!;
        public DateTime? DateTime { get; set; }
    }

    public class CommentDTO
    {
        public long Id { get; set; }
        public string Text { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Name { get; set; } = null!;
        public DateTime? DateTime { get; set; }
    }
}