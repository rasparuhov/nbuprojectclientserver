﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using NBUBackend.Models;

namespace NBUBackend.Models
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; } = null!;
        public DbSet<Comment> Comments { get; set; } = null!;
    }
}